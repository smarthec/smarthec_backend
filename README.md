# smarthec_backend

> AI backend for SmartHEC project: OCR extraction of relevant information from scanned forms via context recognition

Defines a Docker [service](#workspace-processing) that runs an [OCR-D](https://ocr-d.de) [workflow](https://ocr-d.de/en/spec/glossary#ocr-d-workflow) for text extraction of predefined form fields (visual object classes) from scanned/photographed forms on given [OCR-D workspaces](https://ocr-d.de/en/spec/glossary#workspace). The workspace is assumed to contain nothing but a fileGrp `OCR-D-IMG` with the raw images, and will be annotated up to a final fileGrp `OCR-D-OCR-TESS-deu-SEG-tesseract-sparse-FORM-COR` with [PAGE-XML](https://github.com/PRImA-Research-Lab/PAGE-XML) representing the final result. 

In an alternative [API](#direct-processing), all the workspace logic is hidden, so images can be uploaded directly, and the reply will yield corresponding PAGE-XML files.

* [Installation](#installation)
  * [Settings in Docker-Compose](#settings-in-docker-compose)
* [Usage](#usage)
  * [Workspace processing](#workspace-processing)
  * [Direct processing](#direct-processing)
  * [Query PAGE-XML result files](#query-page-xml-result-files)
* [Docker Swarm](#docker-swarm)

## Installation

Install [Docker CE](https://docs.docker.com/install/) and GNU make. Copy `*.h5` (HDF5-formatted) model files trained by [Mask-RCNN for formdata](https://github.com/OCR-D/ocrd_segment/blob/maskrcnn-cli/maskrcnn-cli/formdata.py) into the CWD. Then:

    make build

* For GPU support, also install [Nvidia Runtime Toolkit](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html).
* To run with [Docker Compose](https://docs.docker.com/compose/install/):

        docker-compose pull
        docker-compose up --build -d

  * `--build` to build images before starting containers.
  * `-d` runs in detach mode, leave to get output.

### Settings in Docker-Compose

Customize (or override) settings in `docker-compose.yml`:
- environment variables:
   * `WORKERS`: number of parallel processes on this workflow server
   * `CUDA_WORKERS`: number of GPU workers among `WORKERS` (set 0 to get only CPU workers)
   * `MRCNNPROCS`: number of GPU workers or tasks in total on this device (at least sum of all our `CUDA_WORKERS`)
   * `IMAGES_PER_GPU`: batch size for Tensorflow on GPU workers
   * `MDL`: Messdienstleister model to load on this workflow server

## Usage

### Workspace processing

1. Start one workflow server for each model (i.e. billing company / `Messdienstleister`):

        make run DATA=$PWD MDL=Brunata PORT=7001 &
        make run DATA=$PWD MDL=Techem PORT=7002 &
        make run DATA=$PWD MDL=Ista PORT=7003 &

   > Note: In the future, MDL will be classified on-the-fly, so only 1 model / server will be needed.

2. Create a [workspace](https://ocr-d.de/en/user_guide#preparing-a-workspace) for each group of image files (belonging to one bill) you want to analyse:

        ocrd-import -P brunata-dir1
        ocrd-import -P brunata-dir2
        ocrd-import -P techem-dir
        
   > Note: This could also be created via some [METS](http://www.loc.gov/standards/mets/) template file.

3. For each workspace, issue a processing request to the workflow server. You need to know which model/server to use a priori:

        ocrd workflow client -p 7001 process -m brunata-dir1/mets.xml
        ocrd workflow client -p 7001 process -m brunata-dir2/mets.xml
        ocrd workflow client -p 7002 process -m techem-dir/mets.xml

    (or equivalently:)

        curl -G -d mets=brunata-dir1/mets.xml http://127.0.0.1:7001/process
        curl -G -d mets=brunata-dir2/mets.xml http://127.0.0.1:7001/process
        curl -G -d mets=techem-dir/mets.xml http://127.0.0.1:7002/process

   > Note: Best use a path relative to the `DATA` directory bind-mounted when starting the workflow server.

4. To stop a running server, issue a shutdown request to the workflow server, or stop the respective docker container:

        ocrd workflow client -p 7001 shutdown
        ocrd workflow client -p 7002 shutdown

    (or equivalently:)

        make halt PORT=7001
        make halt PORT=7002

    (or equivalently:)

        curl -G http://127.0.0.1:7001/shutdown
        curl -G http://127.0.0.1:7002/shutdown


### Direct processing

Uses an alternative endpoint, also provided by the OCR-D workflow server, which encapsulates all workspace/METS processing.

* Messdienstleister (Ports, started with `docker-compose`)
  * Techem:    7001
  * Minol:     7002
  * Kalo:      7003
  * BFW:       7004
  * Brunata:   7005
  * Ista:      7006
  * Sonstige:  7007

* `/process_images` Post JSON with base64-encoded images, receive JSON with PAGE-XML and base64-encoded transformed (derived) images; JSON references each page by (arbitrary) name, same for input and output.
  * If `page` or `img` is `null` the page XML/image couldn't be found. Either they weren't created or the configuration is wrong.
  * Request:

        curl -H "Content-Type: application/json" -X POST -d @- http://localhost:7001/process_images <<< "{\"pages\": {\"page01\": \"$(base64 path/to/image1.jpg)\", \"page02\": \"$(base64 path/to/image2.png)\"}}"



  * Response:
    ```json
    {
      "pages": {
        "page01": {
          "page": "PAGE-XML",
          "img": "base64-encoded transformed image"
        },
        "page02": {
          "page": "PAGE-XML",
          "img": "base64-encoded transformed image"
        }
      }
    }
    ```

### Query PAGE-XML result files

To query resulting PAGE-XML (`pc="http://schema.primaresearch.org/PAGE/gts/pagecontent/2019-07-15"`) for
- text of a _context_ annotation for class `gebaeude_heizkosten_raumwaerme`:
    ```xpath
    //*[contains(@custom,"subtype:context=gebaeude_heizkosten_raumwaerme")]/pc:TextEquiv[1]/pc:Unicode/text()
    ```
    
    (`*` because it could be `pc:TextLine` or `pc:Word`)
- respective polygon outline (as white-space separated points, each a comma-separated x/y pair):
    ```xpath
    //*[contains(@custom,"subtype:context=gebaeude_heizkosten_raumwaerme")]/pc:Coords/@points
    ```

    (all coordinates relate to image under `/pc:PcGts/pc:Page/@imageFilename`)
- text of a _target_ annotation for class `gebaeude_heizkosten_raumwaerme`:
    ```xpath
    //pc:TextLine[contains(@custom,"subtype:target=gebaeude_heizkosten_raumwaerme")]/pc:TextEquiv[1]/pc:Unicode/text()
    ```
- respective polygon outline:
    ```xpath
    //pc:TextLine[contains(@custom,"subtype:target=gebaeude_heizkosten_raumwaerme")]/pc:Coords/@points
    ```

    (For targets, `TextLine/Coords` and `TextRegion/Coords` are identical.)
- respective confidence:
    ```xpath
    //pc:TextLine[contains(@custom,"subtype:target=gebaeude_heizkosten_raumwaerme")]/pc:Coords/@conf
    ```
- path name of last derived image on page level (with image preprocessing):
    ```xpath
    /pc:PcGts/pc:Page/pc:AlternativeImage[last()]/@filename
    ```
- respective coordinate transform for that (a 3x3 matrix after `coords=` prefix):
    ```xpath
    /pc:PcGts/pc:Page/@custom
    ```

    (Apply to any segment polygon like [this](https://github.com/OCR-D/core/blob/1df3f456e1284444725a420ba5392c08a86d95aa/ocrd_utils/ocrd_utils/image.py#L131-L134), with the actual transformation [here](https://github.com/OCR-D/core/blob/1df3f456e1284444725a420ba5392c08a86d95aa/ocrd_utils/ocrd_utils/image.py#L304-L319).)


## Docker Swarm

In docker-compose `node.role` configures where the service is deployed.

1. Setup Docker Swarm
  1. Setup manager: [`docker swarm init --advertise-addr IP`](https://docs.docker.com/engine/swarm/swarm-tutorial/create-swarm/).
  2. Add nodes to swarm: [`docker swarm join-token manager`](https://docs.docker.com/engine/swarm/swarm-tutorial/add-nodes/).
2. Setup Docker registry: [`docker service create --name registry --publish published=5000,target=5000 registry:2`](https://docs.docker.com/engine/swarm/stack-deploy/#set-up-a-docker-registry)
3. Add to each service very it should be deployed to, role options could be `manager` or `worker`:

    ```yaml
    deploy:
      placement:
        constraints:
          - node.role == ROLE
    ```

4. Push to registry: `docker-compose push`, might need to run `docker-compuse up -d` and `docker-compose down --volumnes` first.
5. Run with stack deploy: [`docker stack deploy --compose-file docker-compose.yml smarthec`](https://docs.docker.com/engine/swarm/stack-deploy/#deploy-the-stack-to-the-swarm).


Bring down stack with: `docker stack rm smarthec`.
