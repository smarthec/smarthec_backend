# Install by copying (or symlinking) makefiles into a directory
# where all OCR-D workspaces (unpacked BagIts) reside. Then
# chdir to that location.

# Call via:
# `make -f WORKFLOW-CONFIG.mk WORKSPACE-DIRS` or
# `make -f WORKFLOW-CONFIG.mk all` or just
# `make -f WORKFLOW-CONFIG.mk`
# To rebuild partially, you must pass -W to recursive make:
# `make -f WORKFLOW-CONFIG.mk EXTRA_MAKEFLAGS="-W FILEGRP"`
# To get help on available goals:
# `make help`

###
# From here on, custom configuration begins.

info:
	@echo "Read image and create PAGE-XML for it,"
	@echo "then crop, binarize, deskew and denoise pages,"
	@echo "then segment into sparse text regions/lines,"
	@echo "then recognize text with Tesseract model deu,"
	@echo "then classify lines into non/context for forms,"
	@echo "then segment into target regions/lines for forms,"
	@echo "and finally re-OCR new segments into numbers-only."

INPUT = OCR-D-IMG

BIN = $(INPUT)-BINPAGE-sauvola

$(BIN): $(INPUT)
$(BIN): TOOL = ocrd-olena-binarize
$(BIN): PARAMS = "impl": "sauvola-ms-split" # , "k": 0.34 # threshold (larger=thinner)

DEN = $(BIN)-DENOISE-ocropy

# $(DEN): $(BIN)
# $(DEN): TOOL = ocrd-cis-ocropy-denoise
# $(DEN): PARAMS = "level-of-operation": "page", "dpi": 0, "noise_maxsize": 3.0 # max fg/bg noise in pt

FLIP = $(DEN)-DESKEW-tesserocr

# denoising degrades deskewing:
#$(FLIP): $(DEN)
$(FLIP): $(BIN)
$(FLIP): TOOL = ocrd-tesserocr-deskew
$(FLIP): PARAMS = "operation_level": "page", "dpi": 0, "min_orientation_confidence": 3.0

# much more accurate, but too slow:
#DESK = $(FLIP)-DESKEW-ocropy
#
#$(DESK): $(FLIP)
##$(DESK): TOOL = ocrd-dummy
#$(DESK): TOOL = ocrd-cis-ocropy-deskew
#$(DESK): PARAMS = "level-of-operation": "page", "maxskew": 10 # max angle in degrees (larger=slower)

CROP = $(FLIP)-CROP

$(CROP): $(FLIP)
# cannot cope with highly contrastive noise
# (both textual and non-textual):
#$(CROP): TOOL = ocrd-tesserocr-crop
# very fragile w.r.t. skew:
$(CROP): TOOL = ocrd-anybaseocr-crop
$(CROP): PARAMS = "rulerAreaMin": 1.0 # disable ruler detection

OCR = OCR-D-OCR-TESS-deu-SEG-tesseract-sparse

# glyph level for alternatives
$(OCR): $(CROP)
$(OCR): TOOL = ocrd-tesserocr-recognize
$(OCR): PARAMS = "padding": 5, "sparse_text": true, \
		 "segmentation_level": "region", \
		 "textequiv_level": "glyph", \
		 "model": "deu"

TEXT = $(OCR)-FORM-TEXT
$(TEXT): $(OCR)
$(TEXT): TOOL = ocrd-segment-classify-formdata-text
$(TEXT): PARAMS = "threshold": 95, \
		  "num_processes": $(or $(NUM_PROCESSES),4), \
		  "glyph_conf_cutoff": 0.01, \
		  "glyph_topn_cutoff": 5, \
		  "word_topn_cutoff": 10, \
		  "line_topn_cutoff": 20

LAYOUT = $(OCR)-FORM-LAYOUT
$(LAYOUT): $(TEXT)
$(LAYOUT): GPU = 1
$(LAYOUT): TOOL = ocrd-segment-classify-formdata-layout
$(LAYOUT): PARAMS = "model": "$(or $(MDL),Sonstige).h5", \
		    "min_confidence": $(or $(MIN_CONFIDENCE),0.0), \
		    "images_per_gpu": $(or $(IMAGES_PER_GPU),1), \
		    "num_processes": $(or $(NUM_PROCESSES),8)

# re-OCR (only new segments), target-dependent decoding rules
RECOGNIZED = $(OCR)-FORM-OCR
$(RECOGNIZED): $(LAYOUT)
$(RECOGNIZED): TOOL = ocrd-tesserocr-recognize
$(RECOGNIZED): OPTIONS = -P model deu \
	                -P overwrite_segments false \
	                -P overwrite_text false \
			-P textequiv_level glyph \
			-P xpath_parameters \
			'{ "contains(@custom,\"zeitraum\")": \
				{ "tessedit_char_whitelist": "0123456789.,- " }, \
			  "contains(@custom,\"kost\") and not(contains(@custom,\"anteil\"))": \
				{ "tessedit_char_whitelist": "0123456789., €" }, \
			  "contains(@custom,\"einheiten\") or contains(@custom,\"_flaeche\") or contains(@custom,\"_verbrauch\") and not(contains(@custom,\"_einheit\"))": \
				{ "tessedit_char_whitelist": "0123456789.," }, \
			  "contains(@custom,\"temperatur\")": \
				{ "tessedit_char_whitelist": "0123456789" }, \
			  "contains(@custom,\"anteil\")": \
				{ "tessedit_char_whitelist": "0123456789%" }, \
		          "contains(@custom,\"_einheit\")": \
				{ "tessedit_char_whitelist": "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz23²³." } \
			}'

.DEFAULT_GOAL = $(RECOGNIZED)

# Down here, custom configuration ends.
###

include Makefile
