# cf. github.com/OCR-D/ocrd_all
# ocrd/all # ocrd/core # ubuntu:18.04
ARG VERSION=maximum-cuda-git
FROM ocrd/all:$VERSION

MAINTAINER sachunsky@informatik.uni-leipzig.de

# keep PREFIX and VIRTUAL_ENV from ocrd/all
# but export them for COPY etc
ENV PREFIX=$PREFIX
ENV VIRTUAL_ENV=$VIRTUAL_ENV
ENV XDG_DATA_HOME=/usr/share/
ENV RESDIR=/usr/share/ocrd-resources
ENV TESSDATA_PREFIX $RESDIR/ocrd-tesserocr-recognize

# make apt run non-interactive during build
ENV DEBIAN_FRONTEND noninteractive

# make apt system functional
RUN apt-get update && \
    apt-get install -y apt-utils wget git && \
    apt-get clean

WORKDIR /build

RUN ln /usr/bin/python3 /usr/bin/python
RUN ocrd resmgr download ocrd-tesserocr-recognize deu.traineddata
RUN ocrd resmgr download ocrd-tesserocr-recognize eng.traineddata
RUN ocrd resmgr download ocrd-tesserocr-recognize osd.traineddata
# workaround for imgaug#473 (opencv-python/headless)
RUN pip install --no-binary imgaug imgaug
# ensure Tensorflow is configured to use CUDA
RUN pip install tensorflow_gpu==1.15.4
# let h5py match TF 1.15 generated models
RUN pip install h5py==2.10
# remove ocrd_segment from sub-venv
RUN rm $PREFIX/bin/ocrd-segment*
ENV NO_UPDATE=1
# update to sbb_binarization#31 (setup during init)
RUN git -C sbb_binarization fetch origin pull/31/head:setup-init
RUN git -C sbb_binarization checkout setup-init
RUN make -W sbb_binarization ocrd-sbb-binarize
# add ocrd_segment address detection branch
RUN git -C ocrd_segment fetch origin maskrcnn-cli
RUN git -C ocrd_segment checkout maskrcnn-cli
# install in top-level, not in sub-venv
RUN pip install -e ocrd_segment
# update to core#652
RUN git -C core fetch origin pull/652/head:workflow-server
RUN git -C core checkout workflow-server
RUN make -C core install PIP_INSTALL="pip install -e"
# add further workflow configurations
COPY *.mk $PREFIX/share/workflow-configuration/
# add model files for classify-formdata-layout
ENV MRCNNDATA=${RESDIR}/ocrd-segment-classify-formdata-layout
RUN mkdir -p $MRCNNDATA
COPY *.h5 $MRCNNDATA/
# configure writing to ocrd.log for profiling
COPY ocrd_logging.conf /etc
ENV DEBIAN_FRONTEND teletype
# RUN apt-get install -y libatlas3-base liblapack3 libopenblas-base
# RUN apt-get install -y libatlas-base-dev liblapack-dev libopenblas-dev
# RUN pip3 install --force-reinstall --no-binary numpy -v numpy==1.18.5
# RUN ldd /usr/lib/python3.6/site-packages/numpy/core/*.so
# ENV OMP_THREAD_LIMIT=1
RUN for venv in /usr/local/sub-venv/*; do . $venv/bin/activate; make -C core install; done; deactivate

WORKDIR /data
VOLUME /data

# entrypoint is OCR-D workflow server webservice (1 fixed MDL model per server)
# use with `docker run -e MDL=Techem` to override
ENV MDL=Sonstige
# per-page worker timeout in seconds (aborts/reloads when hit)
ENV TIMEOUT=200
# number of parallel worker processes
ENV WORKERS=1
# number of shared GPU clients on this server (should be smaller WORKERS)
ENV CUDA_WORKERS=1
# number of shared GPU clients on this GPU
ENV MRCNNPROCS=3
# workflow configuration makefile to start server for
ENV WORKFLOW=smarthec-sparse-tesseract-deu-postdec
# bad context prediction requires lowering layout prediction threshold
ENV MIN_CONFIDENCE=0.0
CMD make -I /usr/share/workflow-configuration -f /usr/share/workflow-configuration/$WORKFLOW.mk server MDL=$MDL PORT=7001 HOST=0.0.0.0 TIMEOUT=$TIMEOUT WORKERS=$WORKERS MRCNNPROCS=$MRCNNPROCS CUDA_WORKERS=$CUDA_WORKERS MIN_CONFIDENCE=$MIN_CONFIDENCE

# start with `docker run -p N:7001` and query with `ocrd workflow client -p N`
EXPOSE 7001
