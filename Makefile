TAGNAME = bertsky/smarthec_backend

build:
	docker build -t $(TAGNAME) .

run: DATA ?= $(CURDIR)
run: MDL ?= Brunata
run: PORT ?= 7001
run:
	docker run -e MDL=$(MDL) -p $(PORT):7001 -v $(DATA):/data $(TAGNAME)

halt: PORT ?= 7001
halt:
	ocrd workflow client -p $(PORT) shutdown

.PHONY: build run halt
